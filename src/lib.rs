use rust_genius_pdk::{
    create_page, get_resource, match_event, register_resource,
    register_stateful_icon, show_messagebox, create_widget,
};
use serde::Deserialize;
pub use rust_genius_pdk::{event::RawEvent, genius_eventhandler, genius_loadpoint, PluginMetadata, GeniusEvent};

#[derive(GeniusEvent, Deserialize)]
#[event_id = "super cool event"]
#[serde(transparent)]
pub struct SuperCoolEvent(u32);

genius_loadpoint!(crate::load);
genius_eventhandler!(crate::handle_event);

mod icon {
    pub const HEART: &str = "testpl.icon.heart";
}

fn load() -> FnResult<PluginMetadata> {
    register_stateful_icon(icon::HEART, "m480-120-58-52q-101-91-167-157T150-447.5Q111-500 95.5-544T80-634q0-94 63-157t157-63q52 0 99 22t81 62q34-40 81-62t99-22q94 0 157 63t63 157q0 46-15.5 90T810-447.5Q771-395 705-329T538-172l-58 52Zm0-108q96-86 158-147.5t98-107q36-45.5 50-81t14-70.5q0-60-40-100t-100-40q-47 0-87 26.5T518-680h-76q-15-41-55-67.5T300-774q-60 0-100 40t-40 100q0 35 14 70.5t50 81q36 45.5 98 107T480-228Zm0-273Z", "m480-120-58-52q-101-91-167-157T150-447.5Q111-500 95.5-544T80-634q0-94 63-157t157-63q52 0 99 22t81 62q34-40 81-62t99-22q94 0 157 63t63 157q0 46-15.5 90T810-447.5Q771-395 705-329T538-172l-58 52Z")?;

    register_resource("content", "Welcome to your Beloved Tasks!")?;

    create_page(
        "Beloved Tasks",
        "beloved",
        icon::HEART,
        include_str!("main_page.axaml"),
    )?;

    let template = include_str!("widget.axaml");

    create_widget("rust", "Minus", template, template)?;

    show_messagebox(
        "Update available?",
        "Is there an update? I don't know either 😆",
    )?;

    debug!("Registering Resource(42)...");
    register_resource("testpl.my_resource", 42)?;
    debug!("Loading Resource...");
    let res = get_resource::<u32>("testpl.my_resource");
    debug!("Got back: {res:?}");

    info!("Looks like everything went well 👀");

    Ok(PluginMetadata {
        name: "Absolute cool genius plugin".to_owned(),
        version: "∞.0.2-beta".to_owned(),
    })
}

fn handle_event(event: RawEvent) -> FnResult<()> {
    info!("Received event '{}'", event.name);

    match_event! { event;
        SuperCoolEvent as e => {
            info!("Got {} back :>", e.0);
            Ok(())
        },
    }?;

    Ok(())
}

use rust_genius_pdk::__extism as extism_pdk;
use extism_pdk::*;

#[derive(serde::Serialize)]
struct SomeArg {
    a: String,
}

#[extism_pdk::plugin_fn]
pub fn testbtn_click(_: ()) -> FnResult<()> {
    show_messagebox("", "Button Clicked")?;

    Ok(())
}